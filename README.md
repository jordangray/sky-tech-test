Sky developer test
==================

This solution comprises a Koa API (`/api`) and a React SPA (`/app`). Tests are implemented using Mocha and Chai. Both front-end and back-end code is written in ES6 and transpiled using Babel.


How to run
----------

### Setup

Clone the repository and install NPM dependencies.

    git clone git@bitbucket.org:jordangray/sky-tech-test.git
    npm install


### Test that the API is running successfully

    npm run test-api


### Start the API

    npm run api


### Run the client-side application

With the API running, open another command shell in the package root and run:

    npm run app


Now visit one of the following URLs to test customers from different locations:

- [London](http://localhost:3001?sky-test-london)
- [Liverpool](http://localhost:3001?sky-test-liverpool)
- [Unknown location](http://localhost:3001?sky-test-unknown)


Koa API
-------

There are two services:

- The *customer location service* (`api/location`) returns a location ID for any recognised customer ID, and fails if the customer ID does not exist.
- The *catalogue service* (`api/catalogue`) returns a list of available products for a given location ID, and fails if the location ID is omitted or does not exist.

**koa-router** is used to provide the service routes.


### Service structure

Each service has the same structure:

- `{service}Methods.js` provides the service implementation.
- `{service}Routes.js` exports the routes exposed by this service.
- `{service}-spec.js` is the BDD specification for that service.

Separating routes from the service implementation like this serves a few useful purposes:

- It is much easier to spot potential routing conflicts.
- Developers can see what routes a service exposes at a glance.
- The service implementation is cleaner.

This approach becomes increasingly helpful as services increase in scope and expose more operations.


### Testing

Test specifications are written in close proximity to the code being tested. This saves time finding the tests relevant to a specific unit of functionality and encourages a test-first approach by making the specification for a component hard-to-miss.


Application
-----------

The client-side application is built in React, using Redux as a state container.

The app source is in `/app/src`. I used the **React Hot Loader** to speed up development. To save time, and because the CSS was very simple, I directly embedded the stylesheet in the template (`app/assets/template.ejs`).


What I didn't do
----------------

I made several concessions to limited time. Some of the most obvious ones are listed below.


### Connect to a real data source

The API methods are stubbed in this app. If I were doing this for a production app, I would set up a suitable datasource (e.g. SQL, MongoDB) and include a build step to set up a local instance for testing. This seemed unnecessary for a quick technical test, but I am happy to discuss how I would approach this.


### SPA test coverage

I followed a test-first approach for the API, but due to time constraints I didn't make a start for the SPA. This would not be acceptable for a production app.


### Webpack build improvements

I didn't create a webpack production build configuration for the SPA, again because of time constraints. If I had more time, I would have served the compiled assets from webpack through Koa.

Also, because the CSS was very basic, I included it directly in the page template. Normally I would load it directly


### Cross-browser testing

I didn't worry about cross-browser compatibility for this exercise. (My normal standards for production apps are perfect rendering--allowing minor variation due to progressive enhancement--in A-grade browsers and good support in B-grade browsers via graceful degradation.)


### Presentation

Look and feel was explicitly given low priority in this assignment, so the app looks very much like the wireframe.


### Performance

Finally, there are a LOT of front-end performance improvements I omitted in the interest of time. (I am a bit obsessive about performance, so I would *love* to outline some of these in detail!)


Disclosure
----------

I spent more time than specified on this task. This was due to two factors:

1. I'm still learning React, Redux and Koa, so I frequently had to stop to look things up. I did this because I know that Sky are interested in people with React and Node.js experience, so this seemed like a good opportunity to show some of what I've learned.

2. Testing the app on another machine, I realised I had missed the `--save`\`--save-dev` flags for a few packages. It took me a while to realise what I had done wrong and which packages were missing.