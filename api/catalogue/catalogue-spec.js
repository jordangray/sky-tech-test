import { expect } from 'chai';
import supertest from 'co-supertest';

import app from '..';

const request  = supertest.agent(app.listen());
const headers  = { Host: 'api.localhost' };
const API_BASE = '/api/catalogue';

describe(`GET ${ API_BASE }`, () => {

  describe('/:locationID', function () {

    it('returns relevant entries for locationID "LONDON"', function* () {
      const locationID = 'LONDON';
      const response   = yield request.get(`${API_BASE}/${locationID}`).set(headers).end();

      expect(response.status).to.equal(200, response.text);

      expect(response.body).to.have.lengthOf(4)
        .and.to.deep.include.property('[0].forLocationID', locationID);

      expect(response.body).to.deep.include.property('[3].forLocationID', null);
    });

    it('returns relevant entries for locationID "LIVERPOOL"', function* () {
      const locationID = 'LIVERPOOL';
      const response   = yield request.get(`${API_BASE}/${locationID}`).set(headers).end();

      expect(response.status).to.equal(200, response.text);

      expect(response.body).to.have.lengthOf(3)
        .and.to.deep.include.property('[0].forLocationID', locationID);

      expect(response.body).to.deep.include.property('[2].forLocationID', null);
    });

    it('returns default entries for locationID "UNKNOWN"', function* () {
      const locationID = 'UNKNOWN';
      const response   = yield request.get(`${API_BASE}/${locationID}`).set(headers).end();

      expect(response.status).to.equal(200, response.text);

      expect(response.body).to.have.lengthOf(2)
        .and.to.deep.include.property('[0].forLocationID', null);
    });

  });
});
