// Stubbed for the purpose of this test.
const products = [
  { category: 'Sports', product: 'ArsenalTV',       forLocationID: 'LONDON'    },
  { category: 'Sports', product: 'Chelsea TV',      forLocationID: 'LONDON'    },
  { category: 'Sports', product: 'Liverpool TV',    forLocationID: 'LIVERPOOL' },
  { category: 'News',   product: 'Sky News',        forLocationID: null        },
  { category: 'News',   product: 'Sky Sports News', forLocationID: null        }
];

export function* getProductsForLocationID() {
  const locationID = this.params.locationID;

  const matching = products
    .filter(p => !p.forLocationID || p.forLocationID === locationID);

  this.body = matching;
}
