import koa from 'koa';
import koaRouter from 'koa-router';

const app = koa();
const router = koaRouter();

export default app;


// Enable CORS.
import cors from 'koa-cors';
app.use(cors());


// Simple error handling.
function* handleError(next) {
  try {
    yield next;
  } catch (err) {
    this.status = err.status || 500;
    this.body = { error: err.message };
    this.app.emit('error', err, this);
  }
}

app.use(handleError);


// Shorthand to nest routers in our main API router.
const addRoutes = (url, svc) => router.use(url, svc.routes(), svc.allowedMethods());


// Import our service routes
import catalogueRoutes from './catalogue/catalogueRoutes';
import locationRoutes from './location/locationRoutes';

addRoutes('/api/catalogue', catalogueRoutes);
addRoutes('/api/location', locationRoutes);

app.use(router.routes());
app.use(router.allowedMethods());
