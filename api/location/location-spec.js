import { expect } from 'chai';
import supertest from 'co-supertest';

import app from '..';

const request  = supertest.agent(app.listen());
const headers  = { Host: 'api.localhost' };
const API_BASE = '/api/location';

describe(`GET ${ API_BASE }`, () => {

  describe('/:customerID', function () {

    it('returns locationID "LONDON" for a customer in London', function* () {
      const customerID = 'sky-test-london';
      const response   = yield request.get(`${API_BASE}/${customerID}`).set(headers).end();

      expect(response.status).to.equal(200, response.text);
      expect(response.body).to.have.property('locationID', 'LONDON');
    });

    it('returns locationID "LIVERPOOL" for a customer in Liverpool', function* () {
      const customerID = 'sky-test-liverpool';
      const response   = yield request.get(`${API_BASE}/${customerID}`).set(headers).end();

      expect(response.status).to.equal(200, response.text);
      expect(response.body).to.have.property('locationID', 'LIVERPOOL');
    });

    it('returns locationID "UNKNOWN" for a customer with an unknown location', function* () {
      const customerID = 'sky-test-unknown';
      const response   = yield request.get(`${API_BASE}/${customerID}`).set(headers).end();

      expect(response.status).to.equal(200, response.text);
      expect(response.body).to.have.property('locationID', 'UNKNOWN');
    });

    it('returns a 404 status for a non-extant customerID', function* () {
      const customerID = 'sky-test-xxx';
      const response   = yield request.get(`${API_BASE}/${customerID}`).set(headers).end();

      expect(response.status).to.equal(404, response.text);
      expect(response.body).to.have.property('error', `Could not find customerID "${customerID}"`);
    });

  });

});
