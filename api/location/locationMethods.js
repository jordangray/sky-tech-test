// Stubbed for the purpose of this test.
const customers = {
  'sky-test-london': { locationID: 'LONDON' },
  'sky-test-liverpool': { locationID: 'LIVERPOOL' },
  'sky-test-unknown': { locationID: 'UNKNOWN' },
};

export function* getLocationForCustomerID() {
  const customerID = this.params.customerID;

  if (!(customerID in customers)) {
    this.throw(404, `Could not find customerID "${customerID}"`);
  }

  const customer = customers[customerID];
  this.body = customer;
}
