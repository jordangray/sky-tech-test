// Set up the router.
import koaRouter from 'koa-router';
const router = koaRouter();
export default router;

// Import methods being exposed.
import { getLocationForCustomerID } from './locationMethods';

router.get('/:customerID', getLocationForCustomerID);
