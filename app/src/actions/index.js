import fetch from 'isomorphic-fetch';

import es6Promise from 'es6-promise';
es6Promise.polyfill();

export const REQUEST_CATALOGUE = 'REQUEST_CATALOGUE';
export const RECEIVE_CATALOGUE = 'RECEIVE_CATALOGUE';
export const SET_CUSTOMER_ID   = 'SET_CUSTOMER_ID';
export const TOGGLE_IN_BASKET  = 'TOGGLE_IN_BASKET';

export function setCustomerID(customerID) {
  return {
    type: SET_CUSTOMER_ID,
    customerID
  };
}

export function toggleInBasket(product) {
  return {
    type: TOGGLE_IN_BASKET,
    product
  };
}

function requestCatalogue(customerID) {
  return {
    type: REQUEST_CATALOGUE,
    customerID
  };
}

function receiveCatalogue(customerID, json) {
  const categories = json.reduce((cats, item) => {
    const category = item.category;
    cats[category] = cats[category] || [];
    cats[category].push({ product: item.product, isChecked: false });
    return cats;
  }, {});

  return {
    type: RECEIVE_CATALOGUE,
    customerID,
    categories
  };
}

export function fetchCatalogue(customerID) {
  return dispatch => {
    dispatch(requestCatalogue(customerID));
    return fetch(`http://localhost:3000/api/location/${customerID}`)
      .then(response => response.json())
      .then(json => { console.log(json); return json; })
      .then(json => fetch(`http://localhost:3000/api/catalogue/${json.locationID}`))
      .then(response => response.json())
      .then(json => { console.log(json); return json; })
      .then(json => dispatch(receiveCatalogue(customerID, json)));
  };
}

function shouldFetchCatalogue(state) {
  const catalogue = state.catalogue;
  return !catalogue || !catalogue.isFetching;
}

export function fetchCatalogueIfNeeded(customerID) {
  return (dispatch, getState) => {
    if (shouldFetchCatalogue(getState())) {
      return dispatch(fetchCatalogue(customerID));
    }
  };
}
