import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const Basket = ({ items }) => (
  <section className="block catalogue__item">
    <h1>Basket</h1>
    <ul>
      {items.map(item =>
        <li key={item}>{item}</li>
      )}
    </ul>
    <Link to='checkout' className="button" onClick={e => items.length || e.preventDefault()}>Checkout</Link>
  </section>
);

Basket.propTypes = {
  items: PropTypes.array.isRequired
};

export default Basket;
