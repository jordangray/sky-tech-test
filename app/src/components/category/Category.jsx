import React, { PropTypes } from 'react';
import Product from '../product/Product';

const Category = ({ name, products, basket, onProductChange }) => (
  <section className="block catalogue__item">
    <h1>{name}</h1>
    {products.map(item =>
      <Product key={item.product} {...item} isChecked={!!~basket.indexOf(item.product)} onChange={() => onProductChange(item.product)}/>
    )}
  </section>
);

Category.propTypes = {
  name: PropTypes.string.isRequired,
  products: PropTypes.array.isRequired,
  basket: PropTypes.array.isRequired,
  onProductChange: PropTypes.func.isRequired
};

export default Category;
