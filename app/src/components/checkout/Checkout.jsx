import React, { PropTypes } from 'react';
import { connect } from 'react-redux'

const Checkout = ({ customerID, basket }) => (
  <section className="block">
    <h1>Checkout for customer {customerID}</h1>
    <ul>
      {basket.map(item =>
        <li key={item}>{item}</li>
      )}
    </ul>
  </section>
);

Checkout.propTypes = {
  customerID: PropTypes.string.isRequired,
  basket: PropTypes.array.isRequired,
};

const mapStateToProps = state => {
  const { customerID, basket } = state;

  return {
    customerID,
    basket
  };
}

export default connect(mapStateToProps)(Checkout);
