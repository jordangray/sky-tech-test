import React, { PropTypes } from 'react';

const htmlID = product => product.replace(/\W+/g, '-');

const Product = ({ product, onChange, isChecked }) => (
  <div>
    <input type="checkbox" id={htmlID(product)} defaultChecked={isChecked} onChange={onChange}/>
    <label htmlFor={htmlID(product)}>{product}</label>
  </div>
);

Product.propTypes = {
  product: PropTypes.string.isRequired,
  isChecked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired
};

export default Product;
