import React, { Component, PropTypes } from 'react';

import { connect } from 'react-redux'
import { setCustomerID, fetchCatalogueIfNeeded, toggleInBasket } from '../actions'

import Category from '../components/category/Category';
import Basket from '../components/basket/Basket';



const getCookie = name => {
  const value = "; " + document.cookie;
  const parts = value.split(`; name=`);
  if (parts.length == 2) {
    return parts.pop().split(";").shift();
  }
}


class App extends Component {

  componentDidMount() {
    const { customerID, dispatch } = this.props;
    let initCustomerID;

    // Try to get customerID from query string override
    const query = window.location.search;

    if (query.length > 1) {
      initCustomerID = query.slice(1);

    // Try to get customerID from cookie
    } else {
      const fromCookie = getCookie('customerID');
      if (fromCookie && fromCookie.length > 0) {
        initCustomerID = fromCookie;
      }
    }

    if (initCustomerID !== customerID) {
      dispatch(setCustomerID(customerID));
    } else {
      initCustomerID = customerID;
    }

    dispatch(fetchCatalogueIfNeeded(initCustomerID));
  }

  render() {
    const { categories, isFetching, basket, onProductChange } = this.props
    return <main>
      {isFetching && categories.length === 0 &&
        <h1>Loading catalogue</h1>
      }
      {!isFetching && categories.length === 0 &&
        <h1>No products were found</h1>
      }
      {categories.length > 0 &&
        <h1>Select channels</h1>
      }
      <div className="catalogue">
        {Object.keys(categories).sort().map(key =>
          <Category key={key} name={key} products={categories[key]} onProductChange={onProductChange} basket={basket}/>
        )}
        <Basket items={basket}/>
      </div>
    </main>;
  }

}


App.propTypes = {
  customerID: PropTypes.string.isRequired,
  categories: PropTypes.object.isRequired,
  isFetching: PropTypes.bool.isRequired,
  basket: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  const { customerID, catalogue, basket } = state;
  const {  isFetching, categories } = catalogue;

  return {
    customerID,
    categories,
    isFetching,
    basket
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onProductChange: (product) => {
      dispatch(toggleInBasket(product))
    },
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
