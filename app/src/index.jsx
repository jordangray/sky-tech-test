import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';

import store from './store';
import App from './containers/App';
import Checkout from './components/checkout/Checkout';

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path='/' component={App} />
      <Route path='/checkout' component={Checkout}/>
    </Router>
  </Provider>,
  document.getElementById('app')
);
