import { TOGGLE_IN_BASKET } from '../actions';

const toggleItem = (arr, item) => {
  const idx = arr.indexOf(item);
  return !~idx ? arr.concat(item).sort() : arr.filter(val => val !== item);
}

export default function basket(state = [], action) {
  switch (action.type) {

    case TOGGLE_IN_BASKET:
      return  toggleItem(state, action.product);

    default:
      return state;
  }
}
