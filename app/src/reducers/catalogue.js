import { REQUEST_CATALOGUE, RECEIVE_CATALOGUE } from '../actions';

export default function catalogue(state = {
  isFetching: false,
  categories: {}
}, action) {
  switch (action.type) {

    case REQUEST_CATALOGUE:
      return Object.assign({}, state, {
        isFetching: true,
        categories: {}
      });

    case RECEIVE_CATALOGUE:
      return Object.assign({}, state, {
        isFetching: false,
        categories: action.categories
      });

    default:
      return state;
  }
}
