import { SET_CUSTOMER_ID } from '../actions';

export default function customerID(state = null, action) {
  switch (action.type) {

    case 'SET_CUSTOMER_ID':
      return action.customerID;

    default:
      return state;
  }
}
