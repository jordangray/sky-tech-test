import { combineReducers } from 'redux';
import catalogue from './catalogue';
import customerID from './customerID';
import basket from './basket';

const reducer = combineReducers({
  catalogue,
  customerID,
  basket
});

export default reducer;
