import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducers';

import _ from 'lodash';

let state = {
  customerID: 'sky-test-london',
  catalogue: {
    isFetching: false,
    categories: {}
  },
  basket: []
};

const store = createStore(reducer, state, applyMiddleware(thunkMiddleware));

let unsubscribe = store.subscribe(() =>
  console.log('State', store.getState())
);

export default store;
