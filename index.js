import koa from 'koa';
import app from './api';

const init_api = () => {
  app.listen(process.env.PORT || 3000);
};

export default init_api;
